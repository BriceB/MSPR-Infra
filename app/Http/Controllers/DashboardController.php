<?php

namespace App\Http\Controllers;

use App\Models\Informations;
use App\Models\User;
use App\Notifications\InhabitualConnexion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Fortify\Fortify;
use MongoDB\Driver\Session;

class DashboardController extends Controller
{
    public function index(Request $request) {

        $user = User::where('id', Auth::user()->id)->first();
        $informationUser = Informations::where('user_id', Auth::user()->id)->first();

        $userBrowser = $request->server('HTTP_USER_AGENT');
        $userIp = $request->ip();
        $locationUserIp = geoip()->getLocation($userIp);


        if ($informationUser->ip_adress != $userIp) {
            Notification::send($user, new InhabitualConnexion());
        }
        if ($informationUser->default_browser != $userBrowser || $locationUserIp->country != 'United States') {
            DB::table('users')->where('id', auth()->id())->update(['email_verified_at' => null]);
            Informations::where('user_id', auth()->id())->update(['default_browser' => $request->server('HTTP_USER_AGENT')]);
            Fortify::verifyEmailView(function () {
                return view('auth.verify-email');
            });
        } else {
            if ($user->role == 'doctor'){
                $patients = $request->user()->patients()->with('pathologies')->get();
                $users = User::with('pathologies')->get();

                return view('dashboard', [
                    'patients' => $patients,
                    'users' => $users
                ]);
            } elseif ($user->role == 'patient') {
                $pathologies = DB::table('pathologies_users')
                    ->join('pathologies', 'pathologies_users.pathology_id', '=', 'pathologies.id')
                    ->where('pathologies_users.user_id', '=', auth()->id())
                    ->get();

                $patient = DB::table('users')->where('id', '=', auth()->id())->first();
                $doctor = DB::table('users')->where('id', '=', $patient->doctor_id)->first();
                return view('home', [
                    'pathologies' => $pathologies,
                    'doctor' => $doctor
                ]);
            }
        }

    }
}
