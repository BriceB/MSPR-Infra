<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{
    public function editInformation(){
        //SELECT * FROM pathologies
        $pathologies = DB::table('pathologies')->get();

        $patient = DB::table('users')->where('users.id', '=', auth()->id())->first();

        $pathologyId = DB::table('pathologies_users')->where('user_id', auth()->id())->first();

        $userId = auth()->id();

        return view('editPatientInformations', [
            'pathologies' => $pathologies,
            'patient' => $patient,
            'userId' => $userId,
            'pathologyId' => $pathologyId
        ]);
    }

    public function messaging(){
        return view('messaging');
    }
}
