<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pathologie extends Model
{
    use HasFactory;

    public function user() {
        $this->belongsToMany('App\Models\User', 'pathologies_users', 'pathology_id', 'user_id');
    }
}
