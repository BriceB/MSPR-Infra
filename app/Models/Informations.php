<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Informations extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'ip_adress',
        'default_browser',
    ];
    public function users()
    {
        return $this->hasOne(User::class);
    }
}
