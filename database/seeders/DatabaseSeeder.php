<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create(['role'=>'doctor']);
        for ($i=0; $i<50; $i++)
        {
            $doctorId = rand(1, 10);
            User::factory(1)->create(['role'=>'patient', 'doctor_id' => $doctorId]);
        }

        $this->call(PathologieSeeder::class);

        $patients = User::where('role', 'patient')->get();
        foreach ($patients as $patient)
        {
            DB::table('pathologies_users')->insert([
               'pathology_id' => rand(1,6),
               'user_id' => $patient->id
            ]);
        }

        $users = User::all();
        foreach ($users as $user)
        {
            DB::table('informations')->insert([
               'user_id' => $user->id,
               'ip_adress' => '127.0.0.1',
                'default_browser' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'
            ]);
        }

    }
}
