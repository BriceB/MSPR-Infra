<?php

namespace Database\Seeders;

use App\Models\Pathologie;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PathologieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pathologies')->insert([
            'name' => 'Accident vasculaire cérébrale',
            'description' => 'Un accident vasculaire cérébral ou AVC, communément appelé « attaque cérébrale », est une perte soudaine d\'une ou plusieurs fonctions du cerveau.'
        ]);

        DB::table('pathologies')->insert([
            'name' => 'Diabète',
            'description' => 'Le diabète est une maladie chronique qui se caractérise par un excès de sucre dans le sang ou hyperglycémie. Il existe deux principaux types de diabète, dus à des dysfonctionnements différents'
        ]);

        DB::table('pathologies')->insert([
            'name' => 'Cancer du poumon',
            'description' => 'Un cancer du poumon, aussi appelé cancer bronchopulmonaire ou cancer bronchique, est une maladie des cellules des bronches ou plus rarement, des cellules qui tapissent les alvéoles pulmonaires'
        ]);

        DB::table('pathologies')->insert([
            'name' => 'Déficience mentale',
        ]);

        DB::table('pathologies')->insert([
            'name' => 'Maladie de Parkinson',
            'description' => 'La maladie de Parkinson est une maladie chronique dégénérative du système nerveux (ou maladie neurologique). Elle détruit progressivement les neurones à dopamine, dans une zone appelée « substance noire » ou « locus niger » du cerveau.'
        ]);

        DB::table('pathologies')->insert([
            'name' => 'Mucoviscidose',
            'description' => 'La mucoviscidose est une maladie génétique héréditaire caractérisée par l’épaississement des sécrétions de plusieurs organes, essentiellement les poumons et le pancréas, ce qui altère leur fonctionnement.'
        ]);
    }
}
