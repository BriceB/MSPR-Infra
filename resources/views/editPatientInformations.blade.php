@extends('layouts.app')

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form>
                        @csrf
                        <div class="form-group row">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="{{ $patient->email }}">
                        </div>
                        <br>
                        <br>
                        <div class="form-group row">
                            @foreach($pathologies as $pathology)
                                @if($pathology->id === $pathologyId->pathology_id)
                                    <input type="checkbox" id="{{ $pathology->name }}" name="{{ $pathology->name }}" checked>
                                    <label for="{{ $pathology->name }}">{{ $pathology->name }}</label>
                                @else
                                    <input type="checkbox" id="{{ $pathology->name }}" name="{{ $pathology->name }}">
                                    <label for="{{ $pathology->name }}">{{ $pathology->name }}</label>
                                @endif
                            @endforeach
                        </div>
                        <br>
                        <div>
                            <button type="submit" class="btn btn-primary">Enregistrer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
