@extends('layouts.app')

@section('content')
    <h1 class=title style="text-align: center">Mes informations patient</h1>
    <div style="text-align: center">
        <div class="form-group row">
            <b><label for="inputPrenom" class="col-sm-2 col-form-label">Prénom</label></b>
            <div>
                <span>{{ auth()->user()->firstname }}</span>
            </div>
        </div>
        <br>
        <div class="form-group row">
            <b><label for="inputNom" class="col-sm-2 col-form-label" style="text-align: center">Nom</label></b>
            <div>
                <span>{{ auth()->user()->lastname }}</span>
            </div>
        </div>
        <br>
        <div class="form-group row">
            <b><label for="inputDateNaissance" class="col-sm-2 col-form-label" style="text-align: center">Date de naissance</label></b>
            <div>
                <span>{{ auth()->user()->date_of_birth }}</span>
            </div>
        </div>
        <br>
        <div class="form-group row">
            <b><label for="inputDateNaissance" class="col-sm-2 col-form-label" style="text-align: center">Pathologies déclarées</label></b>
            <div>
                @foreach($pathologies as $pathology)
                    <span>{{ $pathology->name }}</span><br>
                @endforeach
            </div>
        </div>
        <br>
        <div class="form-group row" style="text-align: center;">
            <div style="margin-bottom: 10px">
                <a href="editInformation" class="btn btn-primary">
                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                        <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    </svg>
                    Modifier mes informations
                </a>
            </div>
            <br>
            <div>
                <a href="messaging" class="btn btn-success">
                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                        <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    </svg>
                    Contacter mon médecin
                </a>
            </div>
        </div>
    </div>

{{--    <div style="text-align: center">--}}
{{--        <div class="p-6 bg-white border-b border-gray-200">--}}
{{--            <h1 class=title>Information de mon médecin</h1>--}}
{{--            <br>--}}
{{--            <div class="form-group row">--}}
{{--                <b><label for="inputPrenom" class="col-sm-2 col-form-label">Prénom</label></b>--}}
{{--                <div>--}}
{{--                    <span>{{ $doctor->firstname }}</span>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <br>--}}
{{--            <div class="form-group row">--}}
{{--                <b><label for="inputNom" class="col-sm-2 col-form-label">Nom</label></b>--}}
{{--                <div>--}}
{{--                    <span>{{ $doctor->lastname }}</span>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <br>--}}
{{--        </div>--}}
{{--    </div>--}}

@endsection
