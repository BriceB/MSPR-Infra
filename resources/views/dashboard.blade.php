@extends('layouts.app')

@section('content')
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>Liste de mes patients</h1>
{{--                    <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">--}}
{{--                        <div class="grid grid-cols-1 md:grid-cols-3">--}}
{{--                            <div class="p-6">--}}
{{--                                <div class="flex items-center">--}}
{{--                                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">--}}
{{--                                        <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>--}}
{{--                                    </svg>--}}
{{--                                    <div class="ml-4 text-lg leading-7 font-semibold"><p class="text-gray-900 dark:text-white">Mes informations</div>--}}

{{--                                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" class="bi bi-chat-dots" viewBox="0 0 16 16">--}}
{{--                                        <path d="M5 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>--}}
{{--                                        <path d="m2.165 15.803.02-.004c1.83-.363 2.948-.842 3.468-1.105A9.06 9.06 0 0 0 8 15c4.418 0 8-3.134 8-7s-3.582-7-8-7-8 3.134-8 7c0 1.76.743 3.37 1.97 4.6a10.437 10.437 0 0 1-.524 2.318l-.003.011a10.722 10.722 0 0 1-.244.637c-.079.186.074.394.273.362a21.673 21.673 0 0 0 .693-.125zm.8-3.108a1 1 0 0 0-.287-.801C1.618 10.83 1 9.468 1 8c0-3.192 3.004-6 7-6s7 2.808 7 6c0 3.193-3.004 6-7 6a8.06 8.06 0 0 1-2.088-.272 1 1 0 0 0-.711.074c-.387.196-1.24.57-2.634.893a10.97 10.97 0 0 0 .398-2z"/>--}}
{{--                                    </svg>--}}
{{--                                    <a href="home" class="ml-4 text-lg leading-7 font-semibold text-gray-900 dark:text-white">Messagerie</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                    <div>
                        <table style="width: 100%">
                            <thead>
                            <tr>
                                <th scope="col" style="text-align: center">Numéro dossier</th>
                                <th scope="col" style="text-align: center">Nom</th>
                                <th scope="col" style="text-align: center">Prénom</th>
                                <th scope="col" style="text-align: center">Pathologies</th>
                                <th scope="col" style="text-align: center">Consulter ce patient</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($patients as $patient)
                                <tr>
                                    <td style="text-align: center">{{ $patient->id }}</td>
                                    <td style="text-align: center">{{ $patient->lastname }}</td>
                                    <td style="text-align: center">{{ $patient->firstname }}</td>
                                    <td style="text-align: center">
                                        @foreach($patient->pathologies as $pathology)
                                            <p>{{ $pathology->name }}</p>
                                        @endforeach
                                    </td>
                                    <td style="text-align: center">
{{--                                        <a href="/filePatient/{{ $patient->id }}">Consulter le dossier de ce patient</a>--}}
                                        <div style="margin-bottom: 10px">
                                            <a href="/filePatient/{{ $patient->id }}" class="btn btn-primary">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                                                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                                </svg>
                                                Consulter le dossier de ce patient
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{--                            <label for="inputPrenom" class="col-sm-2 col-form-label">Dossiers en cours</label>--}}
                        {{--                            <div class="col-sm-10 list-style-type: none">--}}
                        {{--                                <input type="NumDossier" class="form-control" id="inputEmail3" placeholder="N° Dossier">--}}
                        {{--                                <input type="Nom" class="form-control" id="inputEmail3" placeholder="Nom">--}}
                        {{--                                <input type="Prenom" class="form-control" id="inputEmail3" placeholder="Prénom">--}}
                        {{--                                <input type="Pathologie" class="form-control" id="inputEmail3" placeholder="Pathologie">--}}
                        {{--                                <input type="DateDemande" class="form-control" id="inputEmail3" placeholder="Date de la demande">--}}
                        {{--                            </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
